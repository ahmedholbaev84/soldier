import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Введите ваш возраст: ");
        int age = scanner.nextInt();

        if (age >= 18 && age <= 27) {
            System.out.println("Вы подлежите призыву на срочную службу или можете служить по контракту.");
        } else if (age >= 28 && age <= 59) {
            System.out.println("Вы можете служить по контракту.");
        } else if (age < 18 || age > 59) {
            System.out.println("Вы находитесь в непризывном возрасте.");
        } else {
            System.out.println("Ошибка: указан недопустимый возраст.");
        }

        scanner.close();
    }
}